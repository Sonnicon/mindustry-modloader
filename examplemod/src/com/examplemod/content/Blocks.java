package com.examplemod.content;

import io.anuke.mindustry.game.ContentList;
import io.anuke.mindustry.type.ContentType;
import io.anuke.mindustry.type.ItemStack;
import io.anuke.mindustry.world.Block;
import io.anuke.mindustry.world.blocks.defense.Wall;
import io.anuke.mindustry.world.blocks.production.Smelter;
import io.anuke.mindustry.world.blocks.units.MechPad;

public class Blocks implements ContentList {
    public static Block exampleMechFactory, exampleSmallWall, exampleItemSmelter;

    @Override
    public void load() {
        //Create exampleMechFactory
        exampleMechFactory = new MechPad("examplemod:exampleMechFactory") {
            {
                //turn player into exampleMech
                mech = Mechs.exampleMech;
                size = 2;
            }
        };

        exampleSmallWall = new Wall("examplemod:exampleSmallWall"){
            {
                health = 500;
            }
        };

        exampleItemSmelter = new Smelter("examplemod:exampleSmelter"){{
            health = 100;
            result = Items.exampleItemRefined;
            craftTime = 75f;
            burnDuration = 30f;
            useFlux = true;

            consumes.items(new ItemStack[]{new ItemStack(Items.exampleItemRaw, 1), new ItemStack(io.anuke.mindustry.content.Items.stone, 2)});
            consumes.item(io.anuke.mindustry.content.Items.coal).optional(true);
        }};

    }

    @Override
    public ContentType type() {
        return ContentType.block;
    }
}
