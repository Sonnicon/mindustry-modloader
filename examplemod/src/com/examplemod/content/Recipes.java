package com.examplemod.content;

import io.anuke.mindustry.content.Items;
import io.anuke.mindustry.game.ContentList;
import io.anuke.mindustry.type.Category;
import io.anuke.mindustry.type.ContentType;
import io.anuke.mindustry.type.ItemStack;
import io.anuke.mindustry.type.Recipe;

public class Recipes implements ContentList {
    public static Category exampleCategory;
    @Override
    public void load() {
        //Category:
        //name is used for sprite, and sprites for categories are a todo
        exampleCategory = new Category("zoom");
        //Recipes:
        new Recipe(exampleCategory, Blocks.exampleMechFactory, new ItemStack(com.examplemod.content.Items.exampleItemRefined, 50), new ItemStack(Items.silicon, 200));
        new Recipe(exampleCategory, Blocks.exampleSmallWall, new ItemStack(com.examplemod.content.Items.exampleItemRefined, 12));
        new Recipe(exampleCategory, Blocks.exampleItemSmelter, new ItemStack(Items.densealloy, 150));
    }

    @Override
    public ContentType type() {
        return ContentType.recipe;
    }
}
