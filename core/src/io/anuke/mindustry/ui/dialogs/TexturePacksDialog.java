package io.anuke.mindustry.ui.dialogs;

import io.anuke.mindustry.Vars;
import io.anuke.mindustry.core.Platform;
import io.anuke.mindustry.modloader.objects.TexturePack;
import io.anuke.mindustry.modloader.utils.TexturePackHandler;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Timers;
import io.anuke.ucore.scene.ui.Label;
import io.anuke.ucore.scene.ui.ScrollPane;
import io.anuke.ucore.scene.ui.layout.Table;
import io.anuke.ucore.util.Bundles;

import static io.anuke.mindustry.Vars.*;

public class TexturePacksDialog extends FloatingDialog {

    ScrollPane pane;
    private Table slots;

    public TexturePacksDialog() {
        this("$text.texturepacks");
    }

    public TexturePacksDialog(String title) {
        super(title);

        shown(() -> {
            setup();
            Timers.runTask(2f, () -> Core.scene.setScrollFocus(pane));
        });

        addCloseButton();
    }

    protected void setup() {
        content().clear();

        slots = new Table();
        pane = new ScrollPane(slots);
        pane.setFadeScrollBars(false);
        pane.setScrollingDisabled(true, false);

        slots.marginRight(24);

        Timers.runTask(2f, () -> Core.scene.setScrollFocus(pane));

        for (TexturePack slot : TexturePackHandler.container.getTexturePacks()) {
            Table txtbutton = new Table("pane");
            txtbutton.add("[accent]" + slot.info.name).left().height(20);
            txtbutton.row();

            txtbutton.addImage(slot.icon).size(64, 64).left().expand();

            String color = "[lightgray]";

            txtbutton.add(String.format(Bundles.format("text.editor.author") + " %s",
                    color + (slot.info.author == null ? "Unknown" : slot.info.author))).left().top().pad(0);

            Table t = new Table();
            {
                t.right();

                t.addImageButton(slot.enabled ? "icon-enabled" : "icon-disabled", "emptytoggle", 14 * 3, () -> {
                    slot.enabled = !slot.enabled;
                    setup();
                }).checked(slot.enabled).right();

                t.addImageButton("icon-trash", "empty", 14 * 3, () ->
                        ui.showConfirm("$text.confirm", "$text.texturepack.delete.confirm", () -> {
                            slot.file.delete();
                            TexturePackHandler.removeTexturePack(slot);
                            setup();
                        })
                ).size(14 * 3).right();
            }

            txtbutton.add(t).left();

            txtbutton.row();
            txtbutton.add(new Label(String.format(Bundles.format("text.texturepack.desc", (slot.info.desc == null ? "Unknown" : slot.info.desc)) + " %s",
                    color + (slot.info.desc == null ? "Unknown" : slot.info.desc)))).left().minWidth(650f).wrap();
            txtbutton.row();

            Table l = new Table();
            {
                l.left();

                l.addImageButton("icon-arrow-up", "empty", 14 * 3, () -> {
                    TexturePackHandler.container.swapOrder(slot, "up");
                    setup();
                }).size(14 * 3).left();

                l.row();

                l.addImageButton("icon-arrow-down", "empty", 14 * 3, () -> {
                    TexturePackHandler.container.swapOrder(slot, "down");
                    setup();
                }).size(14 * 3).left();
            }

            slots.add(txtbutton).left().uniformX().fillX().pad(4).padRight(-4).margin(10f).marginLeft(20f).marginRight(20f).width(700f);

            slots.add(l).left().pad(4).margin(10f).marginLeft(20f);

            slots.row();
        }

        content().add(pane);
        content().row();
        addSetup();
    }

    private void addSetup() {
        if (TexturePackHandler.container.getTexturePacks().size == 0) {
            Table t = new Table("pane");
            Label l = new Label("$text.texturepack.none");
            t.add(l);
            slots.add(t.center());
        }

        slots.row();

        if (ios) return;

        Table bottom = new Table(){};

        bottom.addImageTextButton("$text.texturepack.apply", "icon-check", "clear", 14 * 3, () ->
                ui.showConfirm("$text.confirm", "$text.texturepack.apply.confirm", () -> {
                    TexturePackHandler.save();
                    setup();
                })).fillX().margin(10f).minWidth(300f).height(70f).pad(4f).padRight(-4);

        bottom.addImageTextButton("$text.texturepack.import", "icon-add", "clear", 14 * 3, () ->
                Platform.instance.showFileChooser(Bundles.get("text.texturepack.import"), "Texture ZIP", file -> {

                    try {
                        if (!file.toString().equals(Vars.texturePackDirectory + "/" + file.name())) {
                            file.copyTo(Vars.texturePackDirectory);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    TexturePackHandler.registerTexturePacks();
                    setup();

                }, true, "zip")).fillX().margin(10f).minWidth(300f).height(70f).pad(4f).padRight(-4);
        content().add(bottom);
    }
}