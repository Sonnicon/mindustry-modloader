package io.anuke.mindustry.ui.dialogs;

import io.anuke.mindustry.modloader.api.Config;
import io.anuke.mindustry.modloader.utils.Logger;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Timers;
import io.anuke.ucore.scene.ui.ScrollPane;
import io.anuke.ucore.scene.ui.layout.Table;

import java.util.Map;
import java.util.Objects;

public class ConfigDialog extends FloatingDialog {

    private ScrollPane pane;

    public ConfigDialog() { this("$text.mods.config"); }

    public ConfigDialog(String title) {
        super(title);
        addCloseButton();
    }

    protected void setup(String modUUID) {
        if(!Config.configExist(modUUID)) return;
        if(Config.getConfig(modUUID)==null) return;
        Map map = Config.getConfig(modUUID);

        for (Object obj:Objects.requireNonNull(map).keySet()) {
            Table config = new Table();
            pane = new ScrollPane(config, "clear-black");
            pane.setFadeScrollBars(false);
            pane.setScrollingDisabled(true, false);

            Timers.runTask(2f, () -> Core.scene.setScrollFocus(pane));

            config.add(String.format("%s: ", obj.toString()));
            config.addField(map.get(obj).toString(), (text) -> Logger.print("this is so sad.\nCan we hit {0} likes?", text));
            config.row();
        }

        content().add(pane);
    }
}
