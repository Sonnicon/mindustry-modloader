package io.anuke.mindustry.type;

import com.badlogic.gdx.utils.Array;

public class Category {
    public static Array<Category> all = new Array<>();

    public static Category
            turret = new Category("turret"),
            production = new Category("production"),
            distribution = new Category("distribution"),
            liquid = new Category("liquid"),
            power = new Category("power"),
            defense = new Category("defense"),
            crafting = new Category("crafting"),
            units = new Category("units"),
            upgrade = new Category("upgrade"),
    effect = new Category("effect");

    public final byte id;
    public String name;

    public Category(String name) {
        this.name = name;
        id = (byte) all.size;
        all.add(this);
    }
}
