package io.anuke.mindustry.modloader;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;
import io.anuke.mindustry.entities.Player;
import io.anuke.mindustry.modloader.api.EventEmitter;
import io.anuke.mindustry.modloader.utils.SpritePacker;
import io.anuke.ucore.function.BiFunction;

public class ModloaderVars {
    public static final String version = "1.0.1";

    public static final SpritePacker packer = new SpritePacker(2048, 2048, Pixmap.Format.RGBA8888, 2, true);

    public static Array<BiFunction<String, Player, Boolean>> chatInterceptors = new Array<>();

    public static EventEmitter defaultEventEmitter = new EventEmitter();
}
