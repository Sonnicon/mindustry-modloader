package io.anuke.mindustry.modloader.objects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.io.File;

public class Mod {

    public int id;
    public boolean enabled;

    public String uuid;

    public ModInfo modInfo;

    public TextureRegion icon;
    public File file;

    public boolean isClientSided = false;

    public Mod() {
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Mod) {
            return uuid.equals(((Mod) obj).uuid);
        }
        return false;
    }
}
