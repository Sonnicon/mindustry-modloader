package io.anuke.mindustry.modloader.objects;

import com.badlogic.gdx.utils.Array;

public class TexturePackContainer {
    private Array<TexturePack> texturePacks = new Array<>();

    public TexturePackContainer() {}

    public boolean addTexturePack(TexturePack texturePack) {
        if(texturePacks.contains(texturePack, false)) return false;
        texturePacks.add(texturePack);
        return true;
    }

    public boolean removeTexturePack(TexturePack texturePack) {
        if(texturePacks.contains(texturePack, false))
            texturePacks.removeValue(texturePack, false);
        else
            return false;
        return true;
    }

    public Array<TexturePack> getTexturePacks() {
        return texturePacks;
    }

    public TexturePack getTexturePackByName(String name) {
        for (TexturePack m : texturePacks) {
            if(m.info.name.equals(name)) return m;
        }
        return null;
    }

    public void swapOrder(TexturePack tp, String dir){
        try {
            if(!texturePacks.contains(tp, false))
                return;
            Integer ind = texturePacks.indexOf(tp, false);
            texturePacks.swap(ind, dir.equals("up") ? ind - 1 : dir.equals("down") ? ind + 1 : ind);
        } catch(Exception ignored){}
    }

    public void flipContainer(){
        texturePacks.reverse();
    }
}
