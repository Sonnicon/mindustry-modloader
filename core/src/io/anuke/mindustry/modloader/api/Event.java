package io.anuke.mindustry.modloader.api;

public abstract class Event {
  public final String name;

  public Event(String name) {
    this.name = name;
  }
}
