package io.anuke.mindustry.modloader.api.event;

import io.anuke.mindustry.modloader.api.Event;
import io.anuke.mindustry.world.Block;
import io.anuke.mindustry.world.Tile;

public class OnBlockDeconstruct extends Event {

    public Tile tile;
    public Block block;

    public OnBlockDeconstruct(Tile tile, Block block) {
        super("onBlockDeconstruct");
        this.tile = tile;
        this.block = block;
    }
}
