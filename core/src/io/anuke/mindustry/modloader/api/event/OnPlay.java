package io.anuke.mindustry.modloader.api.event;

import io.anuke.mindustry.modloader.api.Event;

public class OnPlay extends Event {
    public OnPlay() {
        super("OnPlay");
    }
}
