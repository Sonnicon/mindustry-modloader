package io.anuke.mindustry.modloader.utils;

import com.badlogic.gdx.utils.Array;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

public class Utils {
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new ArrayList<T>(c);
        java.util.Collections.sort(list);
        return list;
    }

    public static String readerToString(Reader reader) throws IOException {
        char[] arr = new char[8 * 1024];
        StringBuilder buffer = new StringBuilder();
        int numCharsRead;
        while ((numCharsRead = reader.read(arr, 0, arr.length)) != -1) {
            buffer.append(arr, 0, numCharsRead);
        }
        reader.close();
        return buffer.toString();
    }

    public static Array<File> fileWalker(File root, boolean noDirectories) {
        if (root.isDirectory()) {
            if (root.listFiles() == null) return Array.with(root);
            Array<File> files = new Array<>();
            for (File f : root.listFiles()) {
                if(f.isDirectory() && noDirectories)
                    files.addAll(fileWalker(f, true));
                else {
                    if(f.isDirectory()) files.addAll(fileWalker(f, false));
                    else files.add(f);
                }
            }
            return files;
        } else return Array.with(root);
    }

    public static void fileWalkerDo(File root, boolean noDirectories, Consumer<File> ref) {
        for (File f:fileWalker(root, noDirectories)) {
            ref.accept(f);
        }
    }
}
