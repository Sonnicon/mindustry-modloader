package io.anuke.mindustry.modloader.utils;

import com.badlogic.gdx.utils.ObjectMap;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Locale;


public class BundleHelper {
    public static ObjectMap<String, String> bundleBuilder(Reader reader) {
        try {
            ObjectMap<String, String> properties = new ObjectMap<>();
            construct(properties, reader);
            return properties;
        } catch(IOException ignored) {}
        return new ObjectMap<>();
    }

    public static ObjectMap<String, String> getProperties() {
        try {
            Class<?> clasz = Core.bundle.getClass();
            Field field = clasz.getDeclaredField("properties");
            if(!Modifier.isPublic(field.getModifiers())) field.setAccessible(true);
            Object prop = field.get(Core.bundle);
            if(prop instanceof  ObjectMap) {
                return (ObjectMap<String, String>) field.get(clasz);
            } else return null;
        } catch (Exception e) {return null;}
    }

    public static boolean put(String key, String value) {
        try {
            Class<?> clasz = Core.bundle.getClass();
            Field field = clasz.getDeclaredField("properties");
            if(!Modifier.isPublic(field.getModifiers())) field.setAccessible(true);
            Object fieldObject = field.get(Core.bundle);
            Method putMethod = fieldObject.getClass().getDeclaredMethod("put", Object.class, Object.class);
            ReflectionHelper.makeMethodAccessible(putMethod);
            putMethod.invoke(fieldObject, key, value);
            return true;
        } catch (Exception e) {e.printStackTrace(); return false;}
    }

    public static boolean putAll(ObjectMap<String,String> map) {
        try {
            Class<?> clasz = Core.bundle.getClass();
            Field field = clasz.getDeclaredField("properties");
            if(!Modifier.isPublic(field.getModifiers())) field.setAccessible(true);
            Object fieldObject = field.get(Core.bundle);
            Method putAllMethod = fieldObject.getClass().getDeclaredMethod("putAll", ObjectMap.class);
            ReflectionHelper.makeMethodAccessible(putAllMethod);
            putAllMethod.invoke(fieldObject, map);
            return true;
        } catch (Exception e) {e.printStackTrace(); return false;}
    }

    public static Locale getLocale(){
        String loc = Settings.getString("locale");
        if(loc.equals("default")){
            return Locale.getDefault();
        }else{
            Locale lastLocale;
            if(loc.contains("_")){
                String[] split = loc.split("_");
                lastLocale = new Locale(split[0], split[1]);
            }else{
                lastLocale = new Locale(loc);
            }

            return lastLocale;
        }
    }


    // The following is required thanks to one single case
    private static final int NONE = 0, SLASH = 1, UNICODE = 2, CONTINUE = 3, KEY_DONE = 4, IGNORE = 5;

    private static void construct (ObjectMap<String, String> properties, Reader reader) throws IOException {
        if (properties == null) throw new NullPointerException("ObjectMap cannot be null");
        if (reader == null) throw new NullPointerException("Reader cannot be null");
        int mode = NONE, unicode = 0, count = 0;
        char nextChar, buf[] = new char[40];
        int offset = 0, keyLength = -1, intVal;
        boolean firstChar = true;

        BufferedReader br = new BufferedReader(reader);

        while (true) {
            intVal = br.read();
            if (intVal == -1) {
                break;
            }
            nextChar = (char)intVal;

            if (offset == buf.length) {
                char[] newBuf = new char[buf.length * 2];
                System.arraycopy(buf, 0, newBuf, 0, offset);
                buf = newBuf;
            }
            if (mode == UNICODE) {
                int digit = Character.digit(nextChar, 16);
                if (digit >= 0) {
                    unicode = (unicode << 4) + digit;
                    if (++count < 4) {
                        continue;
                    }
                } else if (count <= 4) {
                    throw new IllegalArgumentException("Invalid Unicode sequence: illegal character");
                }
                mode = NONE;
                buf[offset++] = (char)unicode;
                if (nextChar != '\n') {
                    continue;
                }
            }
            if (mode == SLASH) {
                mode = NONE;
                switch (nextChar) {
                    case '\r':
                        mode = CONTINUE; // Look for a following \n
                        continue;
                    case '\n':
                        mode = IGNORE; // Ignore whitespace on the next line
                        continue;
                    case 'b':
                        nextChar = '\b';
                        break;
                    case 'f':
                        nextChar = '\f';
                        break;
                    case 'n':
                        nextChar = '\n';
                        break;
                    case 'r':
                        nextChar = '\r';
                        break;
                    case 't':
                        nextChar = '\t';
                        break;
                    case 'u':
                        mode = UNICODE;
                        unicode = count = 0;
                        continue;
                }
            } else {
                switch (nextChar) {
                    case '#':
                    case '!':
                        if (firstChar) {
                            while (true) {
                                intVal = br.read();
                                if (intVal == -1) {
                                    break;
                                }
                                nextChar = (char)intVal;
                                if (nextChar == '\r' || nextChar == '\n') {
                                    break;
                                }
                            }
                            continue;
                        }
                        break;
                    case '\n':
                        if (mode == CONTINUE) { // Part of a \r\n sequence
                            mode = IGNORE; // Ignore whitespace on the next line
                            continue;
                        }
                        // fall into the next case
                    case '\r':
                        mode = NONE;
                        firstChar = true;
                        if (offset > 0 || (offset == 0 && keyLength == 0)) {
                            if (keyLength == -1) {
                                keyLength = offset;
                            }
                            String temp = new String(buf, 0, offset);
                            properties.put(temp.substring(0, keyLength), temp.substring(keyLength));
                        }
                        keyLength = -1;
                        offset = 0;
                        continue;
                    case '\\':
                        if (mode == KEY_DONE) {
                            keyLength = offset;
                        }
                        mode = SLASH;
                        continue;
                    case '=':
                        if (keyLength == -1) { // if parsing the key
                            mode = NONE;
                            keyLength = offset;
                            continue;
                        }
                        break;
                }

                if (Character.isWhitespace(nextChar)) {
                    if (mode == CONTINUE) {
                        mode = IGNORE;
                    }
                    // if key length == 0 or value length == 0
                    if (offset == 0 || offset == keyLength || mode == IGNORE) {
                        continue;
                    }
                    if (keyLength == -1) { // if parsing the key
                        mode = KEY_DONE;
                        continue;
                    }
                }
                if (mode == IGNORE || mode == CONTINUE) {
                    mode = NONE;
                }
            }
            firstChar = false;
            if (mode == KEY_DONE) {
                keyLength = offset;
                mode = NONE;
            }
            buf[offset++] = nextChar;
        }
        if (mode == UNICODE && count <= 4) {
            throw new IllegalArgumentException("Invalid Unicode sequence: expected format \\uxxxx");
        }
        if (keyLength == -1 && offset > 0) {
            keyLength = offset;
        }
        if (keyLength >= 0) {
            String temp = new String(buf, 0, offset);
            String key = temp.substring(0, keyLength);
            String value = temp.substring(keyLength);
            if (mode == SLASH) {
                value += "\u0000";
            }
            properties.put(key, value);
        }
    }
}
