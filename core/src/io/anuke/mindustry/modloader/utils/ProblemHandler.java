package io.anuke.mindustry.modloader.utils;

import com.badlogic.gdx.files.FileHandle;
import io.anuke.mindustry.Vars;

public class ProblemHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        if(Vars.mobile) {
            //TODO: write me
        } else {
            String userDir = System.getProperty("user.dir");
            FileHandle userDirFile = new FileHandle(userDir.replace("\\", "/"));
            if(userDirFile.isDirectory()) {
                FileHandle crashLog = new FileHandle(userDirFile.path()+"crashlog-"+
                        new java.util.Date().toLocaleString().replace(" ","-"));
                for (StackTraceElement ste:e.getStackTrace()) {
                    crashLog.writeString(ste.toString(), true);
                }
            } else {
                //FAIL AND DIE
                throw new RuntimeException(e);
            }

        }
    }
}
